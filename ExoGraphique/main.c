     #include <SDL2/SDL.h>
     #include <math.h>
     #include <stdio.h>
     #include <string.h>

     /*********************************************************************************************************************/
     /*                              Programme d'exemple de création de rendu + dessin                                    */
     /*********************************************************************************************************************/
     void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
          char const* msg,                                       // message à afficher
          SDL_Window* window,                                    // fenêtre à fermer
          SDL_Renderer* renderer) {                              // renderer à fermer
       char msg_formated[255];                                            
       int l;                                                     
                                            
       if (!ok) {                                                        // Affichage de ce qui ne va pas
     strncpy(msg_formated, msg, 250);                                         
     l = strlen(msg_formated);                                            
     strcpy(msg_formated + l, " : %s\n");                                     
                                            
     SDL_Log(msg_formated, SDL_GetError());                                   
       }                                                          
                                            
       if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
     SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
     renderer = NULL;
       }
       if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
     SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
     window= NULL;
       }
                                            
       SDL_Quit();                                                    
                                            
       if (!ok) {                                       // On quitte si cela ne va pas            
     exit(EXIT_FAILURE);                                                  
       }                                                          
     }                                                        
                                            
     void draw(SDL_Renderer* renderer,int largeur,int hauteur) {                            // Je pense que vous allez faire moins laid :)
                                                
       
           


        SDL_Rect rectangle;
       SDL_SetRenderDrawColor(renderer,0,0,255,255);                           // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque
       rectangle.x = 0;                                             // x haut gauche du rectangle
       rectangle.y = 0;                                                  // y haut gauche du rectangle
       rectangle.w = largeur;                                                // sa largeur (w = width)
       rectangle.h = hauteur;  

       SDL_RenderFillRect(renderer, &rectangle);                        
                                            
                                          
       SDL_SetRenderDrawColor(renderer,255,255,255,255);                           // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque
       rectangle.x = largeur;                                             // x haut gauche du rectangle
       rectangle.y = 0;                                                  // y haut gauche du rectangle
       rectangle.w = largeur;                                                // sa largeur (w = width)
       rectangle.h = hauteur;  

       SDL_RenderFillRect(renderer, &rectangle);         

        SDL_SetRenderDrawColor(renderer,255,0,0,255);                           // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque
       rectangle.x = 2*largeur;                                             // x haut gauche du rectangle
       rectangle.y = 0;                                                  // y haut gauche du rectangle
       rectangle.w = largeur;                                                // sa largeur (w = width)
       rectangle.h = hauteur;  

       SDL_RenderFillRect(renderer, &rectangle);    

            
     }


    void inverseur(int* largeur, int* hauteur,int * inverseur1, int * inverseur2)
    {
        
                if(*largeur > 400)
                {
                    *largeur = 400;
                    *inverseur1 = -1;
                }
                if(*largeur <350)
                {
                    *largeur = 350;
                    *inverseur1 = 1;
                }
                *largeur += *inverseur1;

                if(*hauteur > 800)
                {
                    *hauteur = 800;
                    *inverseur2 = -1;
                }
                if(*hauteur <500)
                {
                    *hauteur = 500;
                    *inverseur2 = 1;
                }
                *hauteur += *inverseur2;
        }

    
  

     void gameLoop( SDL_Renderer * renderer)
     {
        SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
        SDL_Event event;                              // c'est le type IMPORTANT !!
        int largeur=350;
                     int inverseur1 = 1;
                     int inverseur2 = 1;
                     int hauteur = 600;

        while (program_on == SDL_TRUE){
            
            inverseur(&largeur,&hauteur,&inverseur1,&inverseur2);
               
                                     // Voilà la boucle des évènements 
            if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête                                  // de file dans 'event'
                if(event.type == SDL_QUIT){                       // En fonction de la valeur du type de cet évènement
                                              // Un évènement simple, on a cliqué sur la x de la fenêtre
                        program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
                }
            }
            printf("%d\n",largeur);
            SDL_SetRenderDrawColor(renderer,0,0,0,255);      
            SDL_RenderClear(renderer);                     // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque
            draw(renderer,largeur,hauteur);                                      // appel de la fonction qui crée l'image  
            SDL_RenderPresent(renderer); 
            SDL_Delay(10);
        }
      // Affichages et calculs souvent ici
     
     }
        
     int main(int argc, char** argv) {
       (void)argc;
       (void)argv;

       SDL_Window* window = NULL;
       SDL_Renderer* renderer = NULL;

       SDL_DisplayMode screen; renderer = SDL_CreateRenderer(window, -1,
                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

       /*********************************************************************************************************************/  
       /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
       if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

       SDL_GetCurrentDisplayMode(0, &screen);
       printf("Résolution écran\n\tw : %d\n\th : %d\n",
          screen.w, screen.h);

       /* Création de la fenêtre */
       window = SDL_CreateWindow("Premier dessin",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                 screen.h * 0.66,
                 SDL_WINDOW_OPENGL);
       if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

       /* Création du renderer */
       renderer = SDL_CreateRenderer(window, -1,
                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
       if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

       /*********************************************************************************************************************/
       /*                                     On dessine dans le renderer                                                   */
       /*********************************************************************************************************************/
       /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */ 
       gameLoop(renderer);
       
       
                             // affichage
       SDL_Delay(8000);                                     // Pause exprimée en ms
                                                       

       /* on referme proprement la SDL */
       end_sdl(1, "Normal ending", window, renderer);
       return EXIT_SUCCESS;
    }
