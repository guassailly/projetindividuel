#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/
void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
     char const* msg,                                       // message à afficher
     SDL_Window* window,                                    // fenêtre à fermer
     SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                            
  int l;                                                     
                                       
  if (!ok) {                                                        // Affichage de ce qui ne va pas
strncpy(msg_formated, msg, 250);                                         
l = strlen(msg_formated);                                            
strcpy(msg_formated + l, " : %s\n");                                     
                                       
SDL_Log(msg_formated, SDL_GetError());                                   
  }                                                          
                                       
  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
window= NULL;
  }
                                       
  SDL_Quit();                                                    
                                       
  if (!ok) {                                       // On quitte si cela ne va pas            
exit(EXIT_FAILURE);                                                  
  }                                                          
}                                                        
                                       
SDL_Rect** initRectangle(SDL_Renderer* renderer) {                            // Je pense que vous allez faire moins laid :)
                                           
    int largeurprec = 0;
    SDL_Rect** rectangle = malloc(sizeof(SDL_Rect*)*100);
    for(int i = 0; i < 100; i++)
    {
        rectangle[i] = malloc(sizeof(SDL_Rect));
        SDL_SetRenderDrawColor(renderer,144,144,144,255);                           // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque
                                                 // y haut gauche du rectangle
        rectangle[i]->w = rand()%20+5;                                                // sa largeur (w = width)
        rectangle[i]->h = rand()%400;  
        rectangle[i]->x = largeurprec;                                             // x haut gauche du rectangle
        rectangle[i]->y = 800 - rectangle[i]->h;  
        largeurprec=rectangle[i]->w+largeurprec;



        SDL_RenderFillRect(renderer, rectangle[i]);                        
        printf("%d\n",largeurprec);
    }
   return rectangle;
       
}

void drawRectangle(SDL_Renderer* renderer, SDL_Rect** rectangle)
{
                    SDL_SetRenderDrawColor(renderer,144,144,144,255);                           // mode Red, Green, Blue (tous dans 0..255)                         // 0 = transparent ; 255 = opaque

    for(int i = 0; i < 100; i++)
    {

        SDL_RenderFillRect(renderer, rectangle[i]);   
    }
}

void drawBG(SDL_Renderer* renderer)
{


     for (int y = 0; y < 800; y++) {
            float t = (float)(y) / 800;
            Uint8 red = (Uint8)(255 * t);    
            Uint8 blue = 255 - (Uint8)(255 * t);   

            SDL_SetRenderDrawColor(renderer, red, 0, blue, 255);

            // Dessiner une ligne horizontale
            SDL_RenderDrawLine(renderer, 0, y, 2000, y);
        }
}

void drawStars(SDL_Renderer* renderer)
{
    int nbStars = 50;
    SDL_Rect starz[50];
    for(int i = 0; i < nbStars; i++)
    {
        SDL_SetRenderDrawColor(renderer,255,255,255,255);                           
                                                
        starz[i].w = 10;                                         
        starz[i].h = 10;
        starz[i].x = rand()%2000;                                     
        starz[i].y = rand()%200;
            SDL_RenderFillRect(renderer, &starz[i]);                        

    }
}

void freeRectangle(SDL_Rect** rect)
{
    for(int i = 0; i< 100; i ++)
    {
        free(rect[i]);
    }
    free(rect);
}

void drawMoon(SDL_Renderer* renderer, float* angle)
{

    
    int centerX = 1000/2;
    int centerY = 800/2;
    int radius = 800/2 - 40/2;
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    int squareX = centerX - radius * cos(*angle) - 40 / 2;
    int squareY = centerY - radius * sin(*angle) - 40 / 2;
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_Rect squareRect = {squareX, squareY, 40, 40};
    SDL_RenderFillRect(renderer, &squareRect);
    *angle= *angle + 0.01;


}


// void drawMoveBuildings(SDL_Renderer* renderer, SDL_Rect* recttab)
// {
//     for(int i = 0; i < 100; i++)
//     {

//     }
// }

void gameLoop( SDL_Renderer * renderer)
{
    SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
    SDL_Event event;                              // c'est le type IMPORTANT !!
    int largeurPrec = 0;
    float angleLune = 0;
   
    SDL_Rect** rectangle =    initRectangle(renderer);  
    SDL_RenderPresent(renderer); 

    while (program_on == SDL_TRUE){
                 
                                // Voilà la boucle des évènements 
      
     
        drawBG(renderer);

        drawMoon(renderer,&angleLune);
        drawRectangle(renderer,rectangle);
        drawStars(renderer);
        SDL_Delay(50);
        SDL_RenderPresent(renderer); 


         if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête                                  // de file dans 'event'
           if(event.type == SDL_QUIT){                       // En fonction de la valeur du type de cet évènement
                freeRectangle(rectangle);
                    // Un évènement simple, on a cliqué sur la x de la fenêtre
                   program_on = SDL_FALSE;  
           }
       }
   }

}
   
int main(int argc, char** argv) {
  (void)argc;
  (void)argv;
  SDL_Window* window = NULL;
  SDL_Renderer* renderer = NULL;
  SDL_DisplayMode screen; renderer = SDL_CreateRenderer(window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  /*********************************************************************************************************************/  
  /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
  if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
  SDL_GetCurrentDisplayMode(0, &screen);
  printf("Résolution écran\n\tw : %d\n\th : %d\n",
     screen.w, screen.h);
  /* Création de la fenêtre */
  window = SDL_CreateWindow("Premier dessin",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED, 1000,
            800,
            SDL_WINDOW_OPENGL);
  if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
  /* Création du renderer */
  renderer = SDL_CreateRenderer(window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
  /*********************************************************************************************************************/
  /*                                     On dessine dans le renderer                                                   */
  /*********************************************************************************************************************/
  /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */ 
  gameLoop(renderer);
  
                        // affichage
  SDL_Delay(8000);                                     // Pause exprimée en ms
                                                  
  /* on referme proprement la SDL */
  end_sdl(1, "Normal ending", window, renderer);
  return EXIT_SUCCESS;

}