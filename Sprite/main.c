
#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SDL2/SDL_image.h>

/*********************************************************************************************************************/
/*                              Programme d'exemple de création de rendu + dessin                                    */
/*********************************************************************************************************************/
void end_sdl(char ok,                                               // fin normale : ok = 0 ; anormale ok = 1
     char const* msg,                                       // message à afficher
     SDL_Window* window,                                    // fenêtre à fermer
     SDL_Renderer* renderer) {                              // renderer à fermer
  char msg_formated[255];                                            
  int l;                                                     
                                       
  if (!ok) {                                                        // Affichage de ce qui ne va pas
strncpy(msg_formated, msg, 250);                                         
l = strlen(msg_formated);                                            
strcpy(msg_formated + l, " : %s\n");                                     
                                       
SDL_Log(msg_formated, SDL_GetError());                                   
  }                                                          
                                       
  if (renderer != NULL) {                                           // Destruction si nécessaire du renderer
SDL_DestroyRenderer(renderer);                                  // Attention : on suppose que les NULL sont maintenus !!
renderer = NULL;
  }
  if (window != NULL)   {                                           // Destruction si nécessaire de la fenêtre
SDL_DestroyWindow(window);                                      // Attention : on suppose que les NULL sont maintenus !!
window= NULL;
  }
                                       
  SDL_Quit();                                                    
                                       
  if (!ok) {                                       // On quitte si cela ne va pas            
exit(EXIT_FAILURE);                                                  
  }                                                          
}                                                        

void animatorIDLE(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, float *index,float* posX) {
    
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};                     // Rectangle de la vignette en cours dans la planche 
    SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
              &window_dimensions.w,
              &window_dimensions.h);
    SDL_QueryTexture(my_texture,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */
    int nb_images = 8;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 6;                        // zoom, car ces images sont un peu petites
    int offset_x = 64,  // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = 64 ;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    state.w = offset_x;                    // Largeur de la vignette
    state.h = offset_y;   
    destination.w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination.h = offset_y * zoom;       // Hauteur du sprite à l'écran
    destination.x=*posX;
        state.x += ((int)(*index)%nb_images)*offset_x;                 // On passe à la vignette suivante dans l'image
        //state.x %= source.w;   

    *index=(*index+0.05); 
    printf("%f\n",*index);
 
    SDL_RenderCopy(renderer, my_texture, &state,&destination);  
}         
void runRight(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, float *index,float* posX) {
    SDL_Rect 
          source = {0},                    // Rectangle définissant la zone totale de la planche
          window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
          state = {0};                     // Rectangle de la vignette en cours dans la planche 
    SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
              &window_dimensions.w,
              &window_dimensions.h);
    SDL_QueryTexture(my_texture,           // Récupération des dimensions de l'image
             NULL, NULL,
             &source.w, &source.h);
    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */
    int nb_images = 10;                     // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    float zoom = 6;                        // zoom, car ces images sont un peu petites
    int offset_x = 64,  // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = 64 ;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    state.w = offset_x;                    // Largeur de la vignette
    state.h = offset_y;   
    destination.w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination.h = offset_y * zoom;
    destination.x = *posX;
    *posX = *posX+10; 
          // Hauteur du sprite à l'écran
       state.x += ((int)(*index)%nb_images)*offset_x;                // On passe à la vignette suivante dans l'image
        //state.x %= source.w;   
    
    *index=(*index+0.5); 
    //printf("%d\n",*index);
 
    SDL_RenderCopy(renderer, my_texture, &state,&destination);  
}                     
SDL_Texture* textureLoad(char * tex_name,SDL_Renderer * renderer, SDL_Window* window)
{
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture
    my_image = IMG_Load(tex_name);
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);
    SDL_FreeSurface(my_image);
    return my_texture;
}                                         

void gameLoop( SDL_Renderer * renderer, SDL_Window* window)
{
    SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
    SDL_Event event;                              // c'est le type IMPORTANT !!
    SDL_Texture* idleAnim =  textureLoad("spriteSheet.png",renderer,window);
    SDL_Texture* runAnim = textureLoad("runRight.png",renderer,window);
    float index = 0;
    int leftDown = 0;
    float posX = 50;
    while (program_on == SDL_TRUE){
                      
       
        
        printf("Test\n");
      
        

        SDL_RenderClear(renderer);

        if (SDL_PollEvent(&event) && event.type != SDL_MOUSEMOTION ){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête                                  // de file dans 'event'
        //    if(event.type == SDL_QUIT){                       // En fonction de la valeur du type de cet évènement
        //             // Un évènement simple, on a cliqué sur la x de la fenêtre
        //            program_on = SDL_FALSE;  
        //    }

            switch(event.type)
            {
                case SDL_QUIT:
                    program_on = SDL_FALSE;  
                break;
                
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_RIGHT : 
                           leftDown = 1;

                        break;
                    }    
                break;

                case SDL_MOUSEMOTION:
                break;

                case SDL_KEYUP:
                    switch (event.key.keysym.sym) {
                        case SDLK_RIGHT : 
                            printf("ALED\n");
                            leftDown = 0;

                    break;
                    }
                break;

                default :
                   

                break;
                
            }
            
        }else {
            if(!leftDown)
            {
                animatorIDLE(idleAnim,window,renderer,&index,&posX);
            }
            else {
                runRight(runAnim,window,renderer,&index,&posX);
            }

        }
        SDL_RenderPresent(renderer);
        SDL_Delay(10); 
    }
 
}
 
  
int main(int argc, char** argv) {
    (void)argc;
    (void)argv;
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen; renderer = SDL_CreateRenderer(window, -1,
                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n",
       screen.w, screen.h);
    /* Création de la fenêtre */
    window = SDL_CreateWindow("Premier dessin",
              SDL_WINDOWPOS_CENTERED,
              SDL_WINDOWPOS_CENTERED, 1000,
              400,
              SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    /*********************************************************************************************************************/
    /*                                     On dessine dans le renderer                                                   */
    /*********************************************************************************************************************/
    /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */ 

  
    gameLoop(renderer,window);
                              // affichage
    SDL_Delay(8000);                                     // Pause exprimée en ms

    /* on referme proprement la SDL */
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;

}