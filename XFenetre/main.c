     #include <SDL2/SDL.h>
     #include <stdio.h>

     /************************************/
     /*  exemple de création de fenêtres */
     /************************************/

     int main(int argc, char **argv) {
       (void)argc;
       (void)argv;

       SDL_Window* window[40];             // Future fenêtre de droite

      SDL_Window* windowMove =  SDL_CreateWindow("MOVE",0,0,192,108,0);
       /* Initialisation de la SDL  + gestion de l'échec possible */
       if (SDL_Init(SDL_INIT_VIDEO) != 0) {
     SDL_Log("Error : SDL initialisation - %s\n", 
                  SDL_GetError());                // l'initialisation de la SDL a échoué 
     exit(EXIT_FAILURE);
       }

        SDL_DisplayMode current;

        if(SDL_GetCurrentDisplayMode(0, &current) !=0) 
        {           
            exit(EXIT_FAILURE);
        }

        printf(" H : %d, W : %d\n",current.h,current.w);

        int i = 0, j = 0;
       for(int k = 0; k < 20; k++)
        {
            
                window[k] = SDL_CreateWindow("", i, j, 192,108,0);
                i+=current.w/15;
                j+=current.h/15;

        }
        i = 0;
        j = current.h;
        for(int k = 20; k < 40; k++)
        {
            
                window[k]= SDL_CreateWindow("", i, j, 192,108,0);
                i+=current.w/15;
                j-=current.h/15;

        }



      SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
      SDL_Event event;                              // c'est le type IMPORTANT !!


      while (program_on){                           // Voilà la boucle des évènements 
      if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
        int x,y;                                          // de file dans 'event'
        switch(event.type){                       // En fonction de la valeur du type de cet évènement
          case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
          program_on = SDL_FALSE; 
        break;    

        default:
                          // L'évènement défilé ne nous intéresse pas
        break;
    
        
        } 
        for(int k = 0; k < 40; k++)
        {
        SDL_GetWindowPosition(window[k],&x,&y);
        SDL_SetWindowPosition(window[k],x+20,y+20);     
        }   
      }
      // Affichages et calculs souvent ici
     }  


      
       /* Normalement, on devrait ici remplir les fenêtres... */
       SDL_Delay(8000);                           // Pause exprimée  en ms


         for(int k = 0; k < 40; k++)
        {
            SDL_DestroyWindow(window[k]);
        }
       /* et on referme tout ce qu'on a ouvert en ordre inverse de la création */
                // la fenêtre 1     

       SDL_Quit();                                // la SDL
 
       return 0;
     }
